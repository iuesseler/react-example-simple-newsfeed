# Front End NewsFeed Challenge

Very basic news-feed grabs data from api and displays all stories that contain an image, title, and message.
Dropdown filter that displays available titles, and on click showcases matching story.
Built with semantic-ui-react components.
Simple loader.

# Notes:

- Clone this repo. `npm install`, then `npm start` to get your development environment running.
- Node version 9.9.0.
- Make sure CORS is disabled in browser (Chrome extension Allow-Control-Allow-Origin). As api is on separate server and dev environment is not being hosted.

