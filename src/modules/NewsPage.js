import React from 'react';
import { Header, Container, Dimmer, Loader, Grid } from 'semantic-ui-react';
import FilterList from "./FilterList";
import NewsList from "./NewsList";
import axios from 'axios';

class NewsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            newsItems: [],
            activeItemTitle: null
        };
    }

    componentWillMount() {
        axios.get('http://assets.studio71.io/test/news_feed.json').then((result) => {
            const newsItems = result.data;
            const items = newsItems.items;
            this.setState({
                newsItems: items
            })
        })
    }

    onFilterSelect = (activeItemTitle) => {
        this.setState({
            activeItemTitle: activeItemTitle
        });
    }

    render() {
        const { newsItems, activeItemTitle } = this.state;
        const isLoading = newsItems.length > 0 ? false : true;
        const itemsToShow = activeItemTitle ? newsItems.filter(item => item.title === activeItemTitle) : newsItems;
        return (
            <div>
                <Dimmer active={isLoading} >
                    <Loader content='Loading' />
                </Dimmer>
                <Container text style={{ marginTop: '7em' }}>
                    <Header as='h1'>71 Feed</Header>
                    <FilterList items={newsItems} onSelect={ this.onFilterSelect }/>
                    <Grid divided stackable style={{marginTop: '40px'}}>
                        <NewsList items={itemsToShow} activeItemTitle={activeItemTitle}/>
                    </Grid>
                </Container>
            </div>
        );
    }
}

export default NewsPage;
