import React from 'react';
import { Dropdown } from 'semantic-ui-react';

const FilterList = ({ items, onSelect }) => {

    const handleSelect = (e) => {
        const value = e.currentTarget.innerText;
        onSelect(value);
    };

    return (
        <Dropdown text='Filter Posts' icon='filter' labeled button className='icon'>
            <Dropdown.Menu>
            <Dropdown.Divider />
            <Dropdown.Header icon='tags' content='Tag Label' />
            <Dropdown.Menu scrolling>
                {items.map((item, i) => {
                    if (item.title) {
                        return <Dropdown.Item key={i} text={item.title} value={item.title} onClick={e => handleSelect(e)} label={{color: 'red', empty: true, circular: true }}/>;
                    }
                })}
            </Dropdown.Menu>
            </Dropdown.Menu>
        </Dropdown>
    );
}

export default FilterList;
