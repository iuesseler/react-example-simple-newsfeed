import React from 'react';
import { Card, Image } from 'semantic-ui-react';

const NewsList = ({ items }) => {
    const listItems = items.map((item, i) => {
        if (item.title) {
            return (
                <Card key={i} centered>
                    <Image src={item.thumbnail} />
                    <Card.Content>
                        <Card.Header>
                            {item.title}
                        </Card.Header>
                        <Card.Description>
                            {item.message}
                        </Card.Description>
                    </Card.Content>
                </Card>
            );
        }
    });
    return listItems;
}

export default NewsList;
