import React, { Component } from 'react';
import NewsPage from './modules/NewsPage';
import './App.css';

class App extends Component {
  render() {
    return (
    <div className="App">
        <NewsPage/>
    </div>
    );
  }
}

export default App;
